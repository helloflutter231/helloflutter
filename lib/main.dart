import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _HelloFlutterAppState createState() =>  _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String japaneseGreeting = "こんにちはフラッター";

class _HelloFlutterAppState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title:Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(onPressed: (){
              setState(() {
                displayText=displayText == englishGreeting?
                japaneseGreeting:englishGreeting;
              });
            }, icon: Icon(Icons.ac_unit_outlined)),
            IconButton
              (onPressed: (){
              setState(() {
                displayText=displayText == englishGreeting?
                japaneseGreeting:englishGreeting;
              });
            },
                icon: Icon(Icons.account_balance_wallet)),
            IconButton(

                onPressed: (){
                  setState(() {
                    displayText=displayText == englishGreeting?
                    japaneseGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.refresh),

            )

          ],
        ),
        body: Center(
          child: Text(
            displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),


    );

    return Container();
  }
}

